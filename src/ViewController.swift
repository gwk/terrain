// © 2015 George King. All rights reserved.

import AppKit


let heightScale: F32 = 64
let noiseScale: F64 = 1 / 64
let fieldLengthPower = 8
let fieldLength = 1 << fieldLengthPower
let fieldSize = V2I(fieldLength, fieldLength)

let erosionEpsilon: F32 = 0.0001

let thermalErosionMinSlope: F32 = 1 // below this slope soil does not slide.
let thermalErosionTransportRate: F32 = 1 // maximum amount of material leaving a cell each round.
let thermalErosionHysteresis: F32 = 0.1

let waterTransportRate: F32 = 0.5
let waterHysteresis: F32 = 0.1


struct TerrainCell {
  var soilInitial: F32 = 0
  var soil: F32 = 0
  var water: F32 = 0
  var dissolved: F32 = 0 // soil dissolved in water.
  var soilDelta: F32 = 0
  var waterDelta: F32 = 0
  var dissolvedDelta: F32 = 0

  var waterHeight: F32 { return soil + water }

  init(soil: F32 = 0, water: F32 = 0) {
    self.soilInitial = soil
    self.soil = soil
    self.water = water
  }

  func assertIsFinite() {
    assert(soil.isFinite)
    assert(water.isFinite)
    assert(dissolved.isFinite)
    assert(soilDelta.isFinite)
    assert(waterDelta.isFinite)
    assert(dissolvedDelta.isFinite)
  }
}


class ViewController: NSViewController {

  // MARK: NSResponder

  override func keyDown(event: NSEvent) {
    guard let string = event.charactersIgnoringModifiers else {
      super.keyDown(event)
      return
    }
    switch string {
    default: super.keyDown(event)
    }
  }

  // MARK: NSViewController

  override func loadView() {
    view = View(frame: CGRectZero, controller: self)
  }

  override func viewWillAppear() {
    errL("viewWillAppear")
  }

  override func viewDidAppear() {
    errL("viewDidAppear")
    async(self.evolveTerrain)
  }

  // MARK: ViewController

  let random = Random() // random number generator.
  let noise = OSNoise2() // open-simplex noise generator.
  let terrain = AreaBuffer<TerrainCell>(size: fieldSize, val: TerrainCell())
  var step = 0

  let neighborPairs = [
    (V2I(-1, -1), F32(k_sqrt_2)), // NW.
    (V2I( 0, -1), 1),             // NN.
    (V2I( 1, -1), F32(k_sqrt_2)), // NE.
    (V2I( 1,  0), 1),             // EE.
    (V2I( 1,  1), F32(k_sqrt_2)), // SE.
    (V2I( 0,  1), 1),             // SS.
    (V2I(-1,  1), F32(k_sqrt_2)), // SW.
    (V2I(-1,  0), 1),             // WW.
  ]

  required init(coder: NSCoder) { fatalError() }

  init() {
    super.init(nibName: nil, bundle: nil)!
    // generate noise field.
    for c in terrain.allCoords() {
      let x = F64(c.x) * noiseScale
      let y = F64(c.y) * noiseScale
      let h = F32(noise.multiVal(x, y, octaveWeights: [32, 16, 8, 4, 2, 1]) * 0.75 + 0.5) * heightScale
      terrain.setEl(c, TerrainCell(soil: h))
    }
  }

  func erodeThermalNeighborhood(coord: V2I) {
    var cell = terrain.el(coord)
    var sumNeighborTransports: F32 = 0
    var maxTransport: F32 = 0
    for (neighborOffset, neighborDistance) in neighborPairs {
      let neighborCoord = coord + neighborOffset
      let neighbor = terrain.el(neighborCoord)
      let minDelta = thermalErosionMinSlope * neighborDistance
      let delta = cell.soil - neighbor.soil
      if delta <= minDelta { continue }
      let desiredTransport = (delta - minDelta) * 0.5 / neighborDistance // halved because transporting 1 unit creates a delta of 2.
      maxTransport = max(maxTransport, desiredTransport)
      sumNeighborTransports += desiredTransport
    }
    if sumNeighborTransports < erosionEpsilon { return } // protects against divide by zero below.
    let transport = min(maxTransport, thermalErosionTransportRate) // amount leaving cell and distributed to neighbors.
    cell.soilDelta -= transport
    terrain.setEl(coord, cell)
    for (neighborOffset, neighborDistance) in neighborPairs {
      let neighborCoord = coord + neighborOffset
      var neighbor = terrain.el(neighborCoord)
      let minDelta = thermalErosionMinSlope * neighborDistance
      let delta = cell.soil - neighbor.soil
      if delta <= minDelta { continue }
      let desiredTransport = (delta - minDelta) * 0.5 / neighborDistance
      let ratio = desiredTransport / sumNeighborTransports
      neighbor.soilDelta += ratio * transport
      terrain.setEl(neighborCoord, neighbor)
    }
  }

  func erodeThermal() -> Bool {
    // simulates soil sliding due to thermal variations instigating movement and gravity overcoming friction.
    let time = sysTime()
    for coord in terrain.allCoords(inset: 1) {
      erodeThermalNeighborhood(coord)
    }
    var absSoilDelta: F32 = 0
    for (i, var cell) in terrain.enumerate() {
      absSoilDelta += abs(cell.soilDelta)
      cell.soil += cell.soilDelta * (1 - thermalErosionHysteresis)
      cell.soilDelta *= thermalErosionHysteresis
      terrain[i] = cell
    }
    errL("erodeThermal step:\(step) absSoilDelta:\(absSoilDelta) time:\(sysTime() - time)")
    return absSoilDelta < erosionEpsilon // isConverged.
  }


  func erodeHydroNeighborhood(coord: V2I) {
    var cell = terrain.el(coord)
    var sumNeighborWaterTransports: F32 = 0 // sum of squares.
    var maxWaterTransport: F32 = 0
    for (neighborOffset, neighborDistance) in neighborPairs {
      let neighborCoord = coord + neighborOffset
      let neighbor = terrain.el(neighborCoord)
      let waterHeightDelta = cell.waterHeight - neighbor.waterHeight
      if waterHeightDelta <= 0 { continue }
      let desiredWaterTransport = waterHeightDelta * 0.5 / neighborDistance // delta is halved because transporting 1 unit creates a delta of 2.
      maxWaterTransport = max(maxWaterTransport, desiredWaterTransport)
      sumNeighborWaterTransports += desiredWaterTransport.sqr
    }
    let potentialWaterTransport = min(cell.water, maxWaterTransport)
    if potentialWaterTransport < erosionEpsilon { return } // protects against divide by zeros.
    let waterTransport = potentialWaterTransport // min(potentialWaterTransport, waterTransportRate) // amount leaving cell and distributed to neighbors.
    let dissolvedTransport = cell.dissolved * waterTransport / cell.water
    cell.waterDelta -= waterTransport
    cell.dissolvedDelta -= dissolvedTransport
    cell.assertIsFinite()
    terrain.setEl(coord, cell)
    for (neighborOffset, neighborDistance) in neighborPairs {
      let neighborCoord = coord + neighborOffset
      var neighbor = terrain.el(neighborCoord)
      let waterHeightDelta = cell.waterHeight - neighbor.waterHeight
      if waterHeightDelta <= 0 { continue }
      let desiredWaterTransport = waterHeightDelta * 0.5 / neighborDistance // delta is halved because transporting 1 unit creates a delta of 2.
      let ratio = desiredWaterTransport.sqr / sumNeighborWaterTransports
      neighbor.waterDelta += waterTransport * ratio
      neighbor.dissolvedDelta += dissolvedTransport * ratio
      neighbor.assertIsFinite()
      terrain.setEl(neighborCoord, neighbor)
    }
  }

  func drainEdge(coord: V2I) {
    var cell = terrain.el(coord)
    cell.water = 0
    cell.dissolved = 0
    terrain.setEl(coord, cell)
  }

  let evaporationRate: F32 = 0.001
  let maxDissolveRatio: F32 = 0.5
  let dissolveRate: F32 = 0.1
  let rainAmount: F32 = 0.005

  func erodeHydro() -> Bool {
    // simulates soil movement due to movement of water.
    let time = sysTime()
    for (i, var cell) in terrain.enumerate() {
      cell.water += rainAmount * cell.soil / heightScale
      terrain[i] = cell
    }
    for coord in terrain.allCoords(inset: 1) {
      erodeHydroNeighborhood(coord)
    }
    var totalSoil: F32 = 0
    var totalWater: F32 = 0
    var totalDissolved: F32 = 0
    var absWaterDelta: F32 = 0
    var absDissolveDelta: F32 = 0
    var totalEvaporation: F32 = 0
    var totalDissolution: F32 = 0
    var totalDeposition: F32 = 0
    for (i, var cell) in terrain.enumerate() {
      // transfer deltas.
      absWaterDelta += abs(cell.waterDelta)
      absDissolveDelta += abs(cell.dissolvedDelta)
      cell.water += cell.waterDelta * (1 - waterHysteresis)
      cell.waterDelta *= waterHysteresis
      cell.dissolved += cell.dissolvedDelta * (1 - waterHysteresis)
      cell.dissolvedDelta *= waterHysteresis
      // evaporate water.
      let evaporation = min(cell.water, evaporationRate)
      cell.water -= evaporation
      totalEvaporation += evaporation
      // deposit or dissolve soil.
      let maxDissolved = cell.water * maxDissolveRatio
      var dissolveDelta = maxDissolved - cell.dissolved
      if dissolveDelta > 0 {  // dissolve some additional soil.
        dissolveDelta *= dissolveRate
        totalDissolution += dissolveDelta
      } else { // else deposit all excess soil.
        totalDeposition -= dissolveDelta
      }
      cell.dissolved += dissolveDelta
      cell.soil -= dissolveDelta
      cell.soilDelta = -dissolveDelta // for visualization only.
      cell.assertIsFinite()
      terrain[i] = cell
      totalSoil += cell.soil
      totalWater += cell.water
      totalDissolved += cell.dissolved
    }
    for x in 0..<terrain.size.x {
      drainEdge(V2I(x, 0))
      drainEdge(V2I(x, terrain.size.y - 1))
    }
    for y in 0..<terrain.size.x {
      drainEdge(V2I(0, y))
      drainEdge(V2I(terrain.size.x - 1, y))
    }

    err("erodeHydro step:\(step) soil:\(totalSoil) water:\(totalWater) dissolved:\(totalDissolved)")
    err(" absWaterDelta:\(absWaterDelta) absDissolveDelta:\(absDissolveDelta)")
    errL(" evap:\(totalEvaporation) dis:\(totalDissolution) dep:\(totalDeposition) time:\(sysTime() - time)")
    return false // isConverged.
  }

  func evolveTerrain() {
    let isConverged = erodeHydro()
    view.setNeedsDisplay()
    step += 1
    if !isConverged {
      async(self.evolveTerrain)
    } else {
      errL("done")
    }
  }
}
