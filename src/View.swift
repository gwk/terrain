// © 2015 George King. All rights reserved.

import AppKit


class View: NSView {

  weak var controller: ViewController?

  required init(coder: NSCoder) { fatalError() }

  init(frame: CGRect, controller: ViewController) {
    self.controller = controller
    super.init(frame: frame)
  }

  override func drawRect(rect: CGRect) {
    let buffer = controller!.terrain
    let colorBuffer = buffer.map {
      (cell) -> V3U8 in
      let s: F32 = 128
      let delta = cell.soil - cell.soilInitial
      //let delta = cell.soilDelta
      let r: F32 = 0 // -delta * s
      let g: F32 = cell.soil / (heightScale)
      let b: F32 = 0 //cell.water * 16 // -delta * s
      return V3S(r, g, b).toU8Pixel
    }
    let img = CGImage.with(areaBuffer: colorBuffer)
    let ctx = NSGraphicsContext.currentContext()!.CGContext
    ctx.drawImage(img, rect: rect)
  }


}